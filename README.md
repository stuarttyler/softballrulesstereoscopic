
---

# SRS = Softball Rules Stereoscopic

#Welcome to the Softball Rules Steroscopic project

## The idea of this project is to develop visually pleasing assests that promote the learning of Softball rules throughout the universe.

##This will be achieved by providing files that support 3D and time information. This includes videos, games, virtual reality software assets.

##All files are open source under GPL V? This basically means that anyone may use these assets to do what ever they like. Hopefully they end up improving someones knowledge or enjoyment of the game of softball. 


###The source assests are 3D objects developed in Blender v2.79

The output assests can be various in nature but expected to be in the form:

-  video clips (for Youtube channel)
-  3D games (for web based game?)
-  situational play tool - teacher/students interact with touch screen to form plays and discuss outcomes - many standard plays could be saved and shared amoungst other umpires - (web based learning tools) 
-  possibly virtualisation - a game might be made where someone can use a VR helmet to play. (use a VR headset)


### In all deliverables - the basic 3D assets of diamond, equipment, people are all the same. Blender can make videos and 3D applications (games and play tool). 
Blender allows for development of videos in various formats and applications for various platforms.
The 3D assets are managed in Blender.

The programming language used in Blender is Python. 

## Enjoy
Stuart Tyler

Australian Softball Umpire #169

If you are reading this then you know how to contact me - or know someone that does. 




